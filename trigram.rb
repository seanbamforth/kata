class Trigram
  def initialize(filename)
    words = File.read(filename).downcase.scan(/[a-z\.\']+/)
    ngram = Hash.new()
    (words.length - 2).times do |word_no|
      search = words[word_no..word_no+1].join(" ")
      ngram[search] = Array.new() if ngram[search].nil?
      ngram[search] << words[word_no+2] 
    end
    @ngram_hash = ngram
  end 

  def textblock(words,start)
    for i in 0..words
      options = @ngram_hash[start.split[-2..-1].join(" ")] 
      start += " #{options.sample}" if options 
    end 
    return start 
  end
end

trigram = Trigram.new("tomswift.txt")
puts trigram.textblock(200,"what's the")



